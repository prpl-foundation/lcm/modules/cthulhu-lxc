# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.1 - 2024-10-25(15:01:48 +0000)

### Other

- Env variables not shared any more with the containers

## Release v1.0.0 - 2024-10-02(08:41:20 +0000)

### Other

- Implement unprivileged containers

## Release v0.10.0 - 2024-08-01(11:30:43 +0000)

### Other

- Add container execute command capability

## Release v0.9.6 - 2024-03-19(13:55:53 +0000)

### New

- Make container shutdown graceful

## Release v0.9.5 - 2024-01-22(07:52:09 +0000)

## Release v0.9.4 - 2023-12-21(15:01:43 +0000)

### Other

- add notification when container state changes

## Release v0.9.3 - 2023-11-06(09:23:16 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v0.9.2 - 2023-11-06(09:03:57 +0000)

### Other

- cleanup zombies

## Release v0.9.1 - 2023-10-04(13:22:49 +0000)

### Other

- Opensource component

## Release v0.9.0 - 2023-09-28(09:22:22 +0000)

### Other

- Sharing host device fails on sub path

## Release v0.8.0 - 2023-09-25(08:46:49 +0000)

### Other

- add Information command

## Release v0.7.3 - 2023-09-07(10:17:11 +0000)

### Other

- sandboxes were not restored after restart
- Make component available on gitlab.softathome.com

## Release v0.7.2 - 2023-07-26(09:23:42 +0000)

### Other

- create sockets in lxc if they do not exist

## Release v0.7.1 - 2023-07-12(13:19:53 +0000)

### Other

- HostObjects

## Release v0.7.0 - 2023-05-09(14:23:33 +0000)

## Release v0.6.0 - 2023-04-28(09:10:40 +0000)

## Release v0.5.0 - 2023-03-30(07:43:09 +0000)

### Other

- Add missing openwrt dependency on lxc

## Release v0.4.7 - 2023-02-16(20:54:28 +0000)

## Release v0.4.6 - 2023-02-16(19:12:38 +0000)

### Other

- revert graceful shutdown of container

## Release v0.4.5 - 2023-02-09(12:18:09 +0000)

### Other

- stopping state of container was missing

## Release v0.4.4 - 2023-02-07(13:29:36 +0000)

### Fixes

- if a container logs a lot, then the pseudoterminal buffers might fill up

## Release v0.4.3 - 2023-01-30(17:02:31 +0000)

### Other

- SIGCHLD was blocked when starting 2nd container

## Release v0.4.2 - 2022-12-20(16:47:45 +0000)

### Other

- allow mounting different types

## Release v0.4.1 - 2022-07-18(12:29:00 +0000)

### Other

- Start without lxc-start and keep track of process

## Release v0.4.0 - 2022-05-19(09:11:32 +0000)

### New

- enable mounting of files and dirs

## Release v0.3.0 - 2022-05-02(11:54:31 +0000)

### New

- Issue : LCM-54 Logs: Machine Parsable. Execution Unit

## Release v0.2.10 - 2022-04-11(15:58:12 +0000)

### Fixes

- Simplify argv for lxc-start/lxc-execute

## Release v0.2.9 - 2022-02-24(10:34:11 +0000)

## Release v0.2.8 - 2022-02-16(19:49:37 +0000)

## Release v0.2.7 - 2022-02-10(20:49:54 +0000)

## Release v0.2.6 - 2022-01-28(12:33:16 +0000)

### Fixes

-  Multilib (lxcv2 & lxcv4) on target

## Release v0.2.5 - 2021-12-13(17:57:09 +0000)

## Release v0.2.4 - 2021-12-13(09:01:36 +0000)

## Release v0.2.3 - 2021-11-23(08:37:14 +0000)

### Fixes

- remove changes for pihole

## Release v0.2.2 - 2021-11-18(23:00:09 +0000)

## Release v0.2.1 - 2021-10-29(12:18:46 +0000)

### Other

- Implement Network NS support

## Release v0.2.0 - 2021-10-06(18:42:15 +0000)

### New

- support removing of containers

## Release v0.1.4 - 2021-09-27(15:45:32 +0000)

### Fixes

- Run with lxc-start by default

## Release v0.1.3 - 2021-09-24(09:36:19 +0000)

### Fixes

- add mknod for ttyACM0

## Release v0.1.2 - 2021-09-23(12:23:28 +0000)

### Fixes

- execute the ctr if lxc.execute.cmd is defined

### Changes

- mount /dev/shm

## Release v0.1.1 - 2021-09-21(09:48:26 +0000)

### Changes

- add network configuration

## Release v0.1.0 - 2021-09-15(13:24:42 +0000)

### New

- Cgroups support for cthulhu

## Release v0.0.11 - 2021-09-14(15:06:14 +0000)

### Fixes

- Do not fail on missing include dirs

## Release v0.0.10 - 2021-09-13(08:28:46 +0000)

### Fixes

- install seccomp profile

### Other

- Enable auto opensourcing

## Release v0.0.9 - 2021-06-29(08:32:56 +0000)

### Fixes

- Prepare project for opensource

### Changes

- Add FAKEROOT variable to package makefile

### Other

- Correct package name for CI
- Reformat changelog

## Release v0.0.2 2020-12-18(10:39:14 +0100)

### Fix

- Set lib path correctly

## Release v0.0.1 - 2020-12-16(21:46:31 +0000)

### New

- initial release
