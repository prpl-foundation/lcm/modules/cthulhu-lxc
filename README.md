# Cthulhu-lxc module

[[_TOC_]]

## Introduction

This module provides a backend for [cthulhu](https://gitlab.com/soft.at.home/lcm/applications/cthulhu) that allows one to create and control LXC containers.

## Building, installing and testing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    If you have no clue how to do this here are some links that could help you:

    - [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)  <br /><br />

    Make sure you user id is added to the docker group:

    ```
    sudo usermod -aG docker $USER
    ```

1. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/lcm
    ```

    Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/workspace/lcm/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the lcm project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [lib\_amxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [lib\_amxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [lib\_amxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [lib\_amxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [lib\_amxm](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm)
- [lib\_amxj](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj)
- [lib\_sahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)
- [yajl](https://lloyd.github.io/yajl)
- [lxc](https://linuxcontainers.org)

#### Build cthulhu

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the LCM and clone this library in it.

    ```bash
    mkdir -p ~/workspace/lcm/applications
    cd ~/workspace/lcm/applications
    git clone git@gitlab.com:soft.at.home/lcm/applications/cthulhu.git
    ```

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain all the libraries needed for building `libcthulhu`. To be able to build `libcthulhu` you need certain libraries to be installed in the container.

    ```bash
    sudo apt update
    sudo apt install libamxc
    sudo apt install libamxp
    sudo apt install libamxd
    sudo apt install libamxo
    sudo apt install libamxm
    sudo apt install libsahtrace
    sudo apt install libyajl-dev
    sudo apt install lxc-dev

    ```

1. Build it

   In the container:

    ```bash
    cd ~/workspace/lcm/modules/cthulhu-lxc
    make
    ```

### Installing

#### Using make target install

You can install your own compiled version easily in the container by running the install target.

```bash
cd ~/workspace/lcm/modules/cthulhu-lxc
sudo -E make install
```

#### Using package

From within the container you can create packages.

```bash
cd ~/workspace/lcm/modules/cthulhu-lxc
make package
```

The packages generated are:

```
~/workspace/lcm/applications/modules/cthulhu-lxc/cthulhu-lxc-<VERSION>.tar.gz
~/workspace/lcm/applications/modules/cthulhu-lxc/cthulhu-lxc_<VERSION>.deb
```

You can copy these packages and extract/install them.

For ubuntu or debian distributions use dpkg:

```bash
sudo dpkg -i ~/workspace/lcm/modules/cthulhu-lxc/cthulhu-lxc_<VERSION>.deb
```

### Testing

#### Prerequisites

No extra components are needed for testing `cthulhu-lxc`.

#### Run tests

You can run the tests by executing the following command.

```bash
cd ~/workspace/lcm/modules/cthulhu-lxc
make test
```

Or this command if you also want the coverage tests to run:

```bash
cd ~/workspace/lcm/modules/cthulhu-lxc/test
make run coverage
```

#### Coverage reports

The coverage target will generate coverage reports using [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and [gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each file (*.c files) is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are available in the output directory of the compiler used.
Example: using native gcc
When the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML coverage reports can be found at `~/workspace/lcm/applications/cthulhu/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser.
In the container start a python3 http server in background.

```bash
cd ~/workspace/lcm/
python3 -m http.server 8080 &
```

Use the following url to access the reports `http://<IP ADDRESS OF YOUR CONTAINER>:8080/workspace/lcm/modules/cthulhu-lxc/output/<MACHINE>/coverage/report`
You can find the ip address of your container by using the `ip` command in the container.

Example:

```bash
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
173: eth0@if174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:11:00:07 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.7/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 2001:db8:1::242:ac11:7/64 scope global nodad
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:7/64 scope link
       valid_lft forever preferred_lft forever
```

in this case the ip address of the container is `172.17.0.7`.
So the uri you should use is: `http://172.17.0.7:8080/workspace/lcm/modules/cthulhu-lxc/output/x86_64-linux-gnu/coverage/report/`


