/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE 1 // to make strdup work

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <libgen.h>
#include <time.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <yajl/yajl_gen.h>
#include <ctype.h>
#include <linux/limits.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxj/amxj_variant.h>
#include <amxp/amxp_subproc.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_helpers.h>
#include <lxc/lxccontainer.h>
#include <lxc/attach_options.h>
#include <debug/sahtrace.h>
#include <lcm/amxc_data.h>
#include <lcm/lcm_assert.h>
#include <lcm/lcm_worker.h>
#include <lcm/lcm_worker_task.h>
#include <lcm/lcm_serialize.h>

#include "cthulhu_lxc.h"
#include "cthulhu_lxc_instance.h"

#define MOD_CTHULHU "mod_cthulhu"
#define ME "ct_lxc"

// #define LOG_TO_FILE
#define EXEC_OUTPUT_SIZE 128
#define DEFAULT_EXECUTION_TIMEOUT 60

#define SET_ERR(...) SET_LEVEL(ERROR, __VA_ARGS__)
#define SET_INFO(...) SET_LEVEL(INFO, __VA_ARGS__)
#define SET_WARNING(...) SET_LEVEL(WARNING, __VA_ARGS__)

#define SET_LEVEL(level, ...) { \
        SAH_TRACEZ_ ## level(ME, __VA_ARGS__); \
        amxc_string_t msg; \
        amxc_string_init(&msg, 0); \
        amxc_string_setf(&msg, __VA_ARGS__); \
        amxc_var_set(cstring_t, ret, amxc_string_get(&msg, 0)); \
        amxc_string_clean(&msg); }


// Keys used to pass stop data to the asynchronous handler in an htable
#define ASYNC_STOP_TIMEOUT "TimeoutSeconds"
#define ASYNC_STOP_CTRID "ContainerID"

static amxm_shared_object_t* so = NULL;
static amxm_module_t* mod = NULL;
static amxo_parser_t* parser = NULL;
static amxd_object_t* rootobject = NULL;
static amxp_timer_t* update_timer = NULL;
static lcm_worker_t* lcm_worker = NULL;

static int ctr_sig_pipe[2];

static const char* default_include = "/etc/amx/cthulhu-lxc/default.conf";

static void lxc_notif_changed(struct lxc_container* c);
static void update_timer_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv);
static void update_ctr_inst(lxc_instance_t* inst);

static cthulhu_ctr_status_t cthulhu_lxc_status_from_string(const char* str) {
    if(strcmp("STOPPED", str) == 0) {
        return cthulhu_ctr_status_stopped;
    }
    if(strcmp("STARTING", str) == 0) {
        return cthulhu_ctr_status_starting;
    }
    if(strcmp("RUNNING", str) == 0) {
        return cthulhu_ctr_status_running;
    }
    if(strcmp("STOPPING", str) == 0) {
        return cthulhu_ctr_status_stopping;
    }
    if(strcmp("ABORTING", str) == 0) {
        return cthulhu_ctr_status_aborting;
    }
    if(strcmp("FREEZING", str) == 0) {
        return cthulhu_ctr_status_freezing;
    }
    if(strcmp("FROZEN", str) == 0) {
        return cthulhu_ctr_status_frozen;
    }
    if(strcmp("THAWED", str) == 0) {
        return cthulhu_ctr_status_thawed;
    }
    return cthulhu_ctr_status_unknown;
}

static int write_config(const char* file, amxc_var_t* var) {
    int write_length = 0;
    variant_json_t* writer = NULL;
    int fd = 0;

    fd = open(file, O_WRONLY | O_CREAT, 0644);
    if(fd < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create file [%s] (%d: %s)", file, errno, strerror(errno));
        goto exit;
    }

    amxj_writer_new(&writer, var);
    when_null(writer, exit);

    write_length = amxj_write(writer, fd);

exit:
    if(fd) {
        close(fd);
    }
    amxj_writer_delete(&writer);
    return write_length;
}

static amxc_var_t* read_config(const char* file) {
    int read_length = 0;
    int fd = 0;
    amxc_var_t* var = NULL;
    variant_json_t* reader = NULL;

    fd = open(file, O_RDONLY);
    if(fd < 0) {
        SAH_TRACEZ_INFO(ME, "Could not open file [%s] (%d: %s)", file, errno, strerror(errno));
        goto exit;
    }

    amxj_reader_new(&reader);
    when_null(reader, exit);

    read_length = amxj_read(reader, fd);
    while(read_length > 0) {
        read_length = amxj_read(reader, fd);
    }

    var = amxj_reader_result(reader);

exit:
    if(fd) {
        close(fd);
    }
    amxj_reader_delete(&reader);
    return var;
}

static int lxc_create(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    amxc_string_t time_string;
    amxc_string_t tmp_string;
    amxc_string_t cthulhu_data_filename;
    amxc_var_t cthulhu_data;
    FILE* config_file = NULL;
    struct lxc_container* c = NULL;
    const cthulhu_config_t* config = NULL;
    char* config_file_name = NULL;
    char* config_cpy = NULL;
    char* cfg_dir = NULL;

    when_failed(amxc_string_init(&tmp_string, 0), exit);
    when_failed(amxc_string_init(&cthulhu_data_filename, 0), exit);
    when_failed(amxc_string_init(&time_string, 0), exit);
    amxc_var_init(&cthulhu_data);

    config = amxc_var_get_const_cthulhu_config_t(args);
    if(!config) {
        SET_ERR("Config argument is not of type cthulhu_config_t");
        goto exit;
    }
    if(string_is_empty(config->id) || string_is_empty(config->rootfs)) {
        SET_ERR("Config is invalid");
        goto exit;
    }

    c = lxc_container_new(config->id, NULL);
    if(!c) {
        SET_ERR("Failed to setup the lxc_container struct");
        goto exit;
    }
    if(c->is_defined(c)) {
        SAH_TRACEZ_INFO(ME, "Container '%s' already exists", config->id);
        rc = 0;
        goto exit;
    }
    time_t now;
    time(&now);
    // ctime returns a string with \n, so we strip this
    amxc_string_setf(&time_string, "%s", ctime(&now));
    amxc_string_trimr(&time_string, iscntrl);

    SAH_TRACEZ_INFO(ME, "Create LXC container %s", config->id);
    // check if the config dirname exists
    config_file_name = c->config_file_name(c);
    if(string_is_empty(config_file_name)) {
        SET_ERR("Could not get config filename for container %s", config->id);
        goto exit;
    }
    config_cpy = strdup(config_file_name);
    cfg_dir = dirname(config_cpy);
    if(strcmp(cfg_dir, ".") == 0) {
        SET_ERR("Could not extract dir from %s", config_cpy);
        goto exit;
    }
    if(cthulhu_mkdir(cfg_dir, false) < 0) {
        SET_ERR("Could not make directory %s (%d: %s)", cfg_dir, errno, strerror(errno));
        goto exit;
    }

    config_file = fopen(config_file_name, "w");
    if(!config_file) {
        SET_ERR("Could not create config file %s (%d: %s)", c->config_file_name(c), errno, strerror(errno));
        goto exit;
    }
    fprintf(config_file, "# This file is automatically generated with Cthulhu\n");
    fprintf(config_file, "# generated on %s\n\n", time_string.buffer);
    // default include
    fprintf(config_file, "lxc.include = %s\n", default_include);
    // name
    fprintf(config_file, "lxc.uts.name = %s\n", config->id);
    // logging - only enable in lxc > 2
    int lxc_major = atoi(lxc_get_version());
    if(lxc_major > 2) {
        fprintf(config_file, "lxc.log.level = 1\n");
        fprintf(config_file, "lxc.log.syslog = daemon\n");
    }
    // sandbox
    if(string_is_empty(config->cgroup_dir)) {
        fprintf(config_file, "lxc.cgroup.dir = cthulhu/generic\n");
    } else {
        fprintf(config_file, "lxc.cgroup.dir = %s\n", config->cgroup_dir);
    }

    // entrypoint
    amxc_string_reset(&tmp_string);
    if(!amxc_llist_is_empty(&config->entry_point)) {
        amxc_llist_for_each(it, &config->entry_point) {
            amxc_string_appendf(&tmp_string, "%s ", amxc_var_get_const_cstring_t(amxc_var_from_llist_it(it)));
        }
    }
    if(!amxc_llist_is_empty(&config->cmd)) {
        amxc_llist_for_each(it, &config->cmd) {
            amxc_string_appendf(&tmp_string, "%s ", amxc_var_get_const_cstring_t(amxc_var_from_llist_it(it)));
        }
    }
    amxc_string_trim(&tmp_string, NULL);
    if(!amxc_string_is_empty(&tmp_string)) {
        fprintf(config_file, "lxc.init.cmd = %s\n", tmp_string.buffer);
    }
    // cwd
    if(!string_is_empty(config->working_dir)) {
        fprintf(config_file, "lxc.init.cwd = %s\n", config->working_dir);
    }
    // rootfs
    fprintf(config_file, "lxc.rootfs.path = %s\n", config->rootfs);

    // save the bundle name
    amxc_var_set_type(&cthulhu_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&cthulhu_data, "bundle_name", config->bundle_name);
    amxc_var_add_new_key_cstring_t(&cthulhu_data, "bundle_version", config->bundle_version);
    amxc_var_add_new_key_cstring_t(&cthulhu_data, "sb_id", config->sandbox);
    amxc_var_add_new_key_cstring_t(&cthulhu_data, "create_time", time_string.buffer);
    amxc_string_setf(&cthulhu_data_filename, "%s/cthulhu.json", cfg_dir);
    write_config(cthulhu_data_filename.buffer, &cthulhu_data);

    rc = 0;
exit:
    free(config_file_name);
    free(config_cpy);
    if(config_file) {
        fclose(config_file);
    }
    amxc_string_clean(&tmp_string);
    amxc_string_clean(&time_string);
    amxc_string_clean(&cthulhu_data_filename);
    amxc_var_clean(&cthulhu_data);
    lxc_container_put(c);
    amxc_var_set(bool, ret, rc);
    return rc;
}

static void lxc_stopped_handler(int fd, UNUSED void* priv) {
    pid_t pid;
    lxc_instance_t* inst;

    if(fd == 0) {
        SAH_TRACEZ_ERROR(ME, "no read fd");
        goto exit;
    }

    // empty read buffer
    if(read(fd, &pid, sizeof(pid_t)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to read from pipe (%d: %s)", errno, strerror(errno));
    }
    if(pid > 0) {
        cthulhu_wait_for_pid(pid);
    }
    inst = cthulhu_lxc_instance_find_by_pid(pid);
    when_null(inst, exit);

    SAH_TRACEZ_INFO(ME, "Container [%s] stopped running", inst->ctr_name);
    update_ctr_inst(inst);
exit:
    return;
}

static int start_container(struct lxc_container* c) {
    int res = -1;
    pid_t child_pid;
    lxc_instance_t* lxc_instance = NULL;

    child_pid = fork();
    if(child_pid < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to fork");
        goto exit;
    }
    if(child_pid == 0) {
        // forked process
        signal(SIGINT, SIG_IGN);
        signal(SIGTERM, SIG_IGN);
        signal(SIGABRT, SIG_IGN);
        pid_t my_pid = getpid();
        c->want_daemonize(c, true);
        res = c->start(c, 0, NULL);
        SAH_TRACEZ_INFO(ME, "Container started: %d", res);
        if(res) {
            // container started, wait for it to stop
            c->wait(c, "STOPPED", -1);
        }
        lxc_container_put(c);
        if(ctr_sig_pipe[1] != 0) {
            if(write(ctr_sig_pipe[1], &my_pid, sizeof(pid_t)) != sizeof(pid_t)) {
                SAH_TRACEZ_ERROR(ME, "Failed to write to sig pipe");
            }
        }
        if(res) {
            _exit(EXIT_SUCCESS);
        }
        _exit(EXIT_FAILURE);
    }
    lxc_instance = cthulhu_lxc_instance_find_by_name(c->name);
    if(!lxc_instance) {
        cthulhu_lxc_instance_new(&lxc_instance);
        lxc_instance->ctr_name = strdup(c->name);
    }
    lxc_instance->status = cthulhu_ctr_status_starting;

    res = 0;
exit:
    return res;
}

static int lxc_start(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    struct lxc_container* c = NULL;
    const char* ctr_id = NULL;
    char* exec_cmd = NULL;
    amxc_var_t* data_var = NULL;
    char* config_file_name = NULL;
    const cthulhu_ctr_start_data_t* start_data = NULL;
    amxc_string_t exec_cmd_str;
    amxc_string_init(&exec_cmd_str, 0);
    amxc_string_t devices_filename;
    amxc_string_init(&devices_filename, 0);
    char buffer[1024];

    exec_cmd = (char*) calloc(1, PATH_MAX);

    ctr_id = GET_CHAR(args, CTHULHU_PLUGIN_START_CTRID);
    if(string_is_empty(ctr_id)) {
        SET_ERR("ContainerId is not set");
        goto exit;
    }
    data_var = GET_ARG(args, CTHULHU_PLUGIN_START_DATA);
    if(data_var) {
        start_data = amxc_var_get_const_cthulhu_ctr_start_data_t(data_var);
    }

    when_null(exec_cmd, exit);

#ifdef LOG_TO_FILE
    struct lxc_log log;
    log.name = ctr_id;
    log.file = "/tmp/ctrlog";
    log.level = "DEBUG";
    log.prefix = "";
    log.quiet = false;
    log.lxcpath = "/var/run/lxc/containers";
    if(lxc_log_init(&log)) {
        SAH_TRACEZ_ERROR(ME, "Could not init log");
    }
#endif

    c = lxc_container_new(ctr_id, NULL);
    if(!c) {
        SET_ERR("Failed to setup the lxc_container struct");
        goto exit;
    }
    if(!c->may_control(c)) {
        SET_ERR("Insufficent privileges to control %s", ctr_id);
        goto exit;
    }

    if(!c->is_defined(c)) {
        SET_ERR("Container '%s' does not exist", ctr_id);
        goto exit;
    }
    if(c->is_running(c)) {
        SAH_TRACEZ_INFO(ME, "the container '%s' is already running", ctr_id);
        rc = 0;
        goto exit;
    }
    if((config_file_name = c->config_file_name(c)) == NULL) {
        SET_ERR("Container '%s' could not get config file name", ctr_id);
        goto exit;
    }
    if(data_var) {
        // mount points
        c->clear_config_item(c, "lxc.mount.entry");

        amxc_llist_for_each(it, &start_data->mounts) {
            cthulhu_mount_data_t* mount_data = amxc_llist_it_get_data(it, cthulhu_mount_data_t, lit);
            if(string_is_empty(mount_data->source) || string_is_empty(mount_data->destination)) {
                continue;
            }
            char* destination = mount_data->destination;
            // for lxc, the destination should be defined without slash
            if(destination[0] == '/') {
                destination++;
            }
            amxc_string_t type;
            amxc_string_init(&type, 0);
            if(!string_is_empty(mount_data->type)) {
                amxc_string_appendf(&type, "%s", mount_data->type);
            } else {
                amxc_string_appendf(&type, "none");
            }
            amxc_string_t options;
            amxc_string_init(&options, 0);
            if(!string_is_empty(mount_data->options)) {
                amxc_string_appendf(&options, "%s", mount_data->options);
            } else {
                amxc_string_appendf(&options, "bind");
                if(cthulhu_isdir(mount_data->source)) {
                    amxc_string_appendf(&options, ",create=dir");
                } else if(cthulhu_isfile(mount_data->source) || cthulhu_issocket(mount_data->source)) {
                    amxc_string_appendf(&options, ",create=file");
                }
            }
            snprintf(buffer, sizeof(buffer) - 1, "%s %s %s %s 0 0",
                     mount_data->source, destination, type.buffer, options.buffer);
            amxc_string_clean(&type);
            amxc_string_clean(&options);
            if(!c->set_config_item(c, "lxc.mount.entry", buffer)) {
                SAH_TRACEZ_INFO(ME, "could not set lxc mount %s", buffer);
            }
        }

        // user mapping
        c->clear_config_item(c, "lxc.idmap");
        amxc_llist_for_each(it, &start_data->usermappings) {
            cthulhu_usermapping_data_t* usermapping_data = amxc_llist_it_get_data(it, cthulhu_usermapping_data_t, lit);
            snprintf(buffer, sizeof(buffer) - 1, "%c %u %u %u",
                     (usermapping_data->type == cthulhu_usermapping_user) ? 'u' : 'g',
                     usermapping_data->ctr_id, usermapping_data->host_id, usermapping_data->range);
            if(!c->set_config_item(c, "lxc.idmap", buffer)) {
                SAH_TRACEZ_INFO(ME, "could not set config flag lxc.idmap");
            }
        }
        // create a mountdir owned by the user that lxc can use for mounting
        if(start_data->run_user_id > 0) {
            sprintf(buffer, "%s/mountdir", start_data->host_data_dir);
            cthulhu_mkdir(buffer, false);
            if(chown(buffer, start_data->run_user_id, start_data->run_group_id) < 0) {
                SAH_TRACEZ_ERROR(ME, "Could not change ownership of %s (%d: %s)", buffer, errno, strerror(errno));
            }
            c->clear_config_item(c, "lxc.rootfs.mount");
            if(!c->set_config_item(c, "lxc.rootfs.mount", buffer)) {
                SAH_TRACEZ_INFO(ME, "could not set config flag lxc.rootfs.mount");
            }
        }

        c->clear_config_item(c, "lxc.cap.keep");
        if(!string_is_empty(start_data->capabilities)) {
            // transform capabilities to lxc format:
            // all lowercase
            // remove the CAP_ prefix
            amxc_string_t caps;
            amxc_string_init(&caps, 0);
            amxc_string_set(&caps, start_data->capabilities);
            amxc_string_to_lower(&caps);
            amxc_string_replace(&caps, "cap_", "", UINT32_MAX);

            CHECK_FALSE_LOG(INFO, c->set_config_item(c, "lxc.cap.keep", caps.buffer), ,
                            "Could not set config flag lxc.cap.keep");
            amxc_string_clean(&caps);
        }

        // EnvironmentVariables
        c->clear_config_item(c, "lxc.environment");
        amxc_llist_for_each(it, &start_data->envvar) {
            cthulhu_envvar_data_t* envvar_data = amxc_llist_it_get_data(it, cthulhu_envvar_data_t, lit);
            if(string_is_empty(envvar_data->key) || string_is_empty(envvar_data->value)) {
                continue;
            }
            snprintf(buffer, sizeof(buffer) - 1, "%s=%s", envvar_data->key, envvar_data->value);
            if(!c->set_config_item(c, "lxc.environment", buffer)) {
                SAH_TRACEZ_ERROR(ME, "could not set lxc environment variables %s", buffer);
            }
        }

        // sandbox data
        cthulhu_sb_data_t* sb_data = start_data->sb_data;
        if(sb_data) {
            snprintf(buffer, sizeof(buffer) - 1, "%d", sb_data->ns_pid);
            if(sb_data->ns_inherrit_uts) {
                SAH_TRACEZ_INFO(ME, "Container '%s' inherrit UTS from %d", ctr_id, sb_data->ns_pid);
                c->clear_config_item(c, "lxc.namespace.share.uts");
                if(!c->set_config_item(c, "lxc.namespace.share.uts", buffer)) {
                    SAH_TRACEZ_INFO(ME, "could not set config flag lxc.namespace.share.uts");
                }
            }
            if(sb_data->ns_inherrit_net) {
                SAH_TRACEZ_INFO(ME, "Container '%s' inherrit NET from %d", ctr_id, sb_data->ns_pid);
                c->clear_config_item(c, "lxc.namespace.share.net");
                if(!c->set_config_item(c, "lxc.namespace.share.net", buffer)) {
                    SAH_TRACEZ_INFO(ME, "could not set config flag lxc.namespace.share.net");
                }
            }
        }
    }
    c->save_config(c, NULL);
    // reload the config
    // https://github.com/lxc/lxc/issues/4356
    lxc_container_put(c);
    c = lxc_container_new(ctr_id, NULL);

    if(start_container(c)) {
        SET_ERR("Container '%s' could not be started", ctr_id);
        goto exit;
    }

    rc = 0;
exit:
    free(exec_cmd);
    free(config_file_name);
    amxc_string_clean(&exec_cmd_str);
    amxc_string_clean(&devices_filename);
    if(c) {
        lxc_container_put(c);
    }
    return rc;
}

static int shutdown_task_func(UNUSED lcm_worker_t* worker, amxc_var_t* var, UNUSED int rc, UNUSED amxc_var_t* data) {
    int res = -1;
    uint32_t timeout = 0;
    const char* ctr_id = NULL;
    struct lxc_container* c = NULL;

    ctr_id = GET_CHAR(var, ASYNC_STOP_CTRID);
    ASSERT_STR_NOT_EMPTY(ctr_id, goto exit);
    timeout = GET_UINT32(var, ASYNC_STOP_TIMEOUT);
    ASSERT_NOT_EQUAL(timeout, 0, goto exit);

    c = lxc_container_new(ctr_id, NULL);
    if(!c) {
        SAH_TRACEZ_ERROR(ME, "unable to open LXC container %s for stopping", ctr_id);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "shutting down container %s", c->name);
    if(!c->shutdown(c, timeout)) {
        SAH_TRACEZ_WARNING(ME, "shutdown for container %s failed, resorting to stop", c->name);
        if(!c->stop(c)) {
            SAH_TRACEZ_ERROR(ME, "stop for container %s failed", c->name);
        }
    }
    // check if the container really stopped
    // some containers keep on running when the init process is killed
    if(cthulhu_lxc_status_from_string(c->state(c)) != cthulhu_ctr_status_stopped) {
        SAH_TRACEZ_WARNING(ME, "Shutdown did not really stop the container. Force stop it");
        if(!c->stop(c)) {
            SAH_TRACEZ_ERROR(ME, "stop for container %s failed", c->name);
        }
    }

    res = 0;
exit:
    if(c) {
        lxc_container_put(c);
    }
    return res;
}

static bool submit_shutdown_task(uint32_t shutdown_timeout, const char* ctr_id) {
    lcm_worker_task_t* stop_task = NULL;
    amxc_var_t* async_stop_data = NULL;

    when_failed(amxc_var_new(&async_stop_data), exit);
    when_failed(amxc_var_set_type(async_stop_data, AMXC_VAR_ID_HTABLE), exit);
    when_null(amxc_var_add_key(uint32_t, async_stop_data, ASYNC_STOP_TIMEOUT, shutdown_timeout), exit);
    when_null(amxc_var_add_key(cstring_t, async_stop_data, ASYNC_STOP_CTRID, ctr_id), exit);

    when_failed(lcm_worker_task_new(&stop_task), exit);
    when_failed(lcm_worker_task_add_function(stop_task, shutdown_task_func, async_stop_data), exit);
    when_failed(lcm_worker_add_task(lcm_worker, stop_task, false), exit);

    return true;

exit:
    amxc_var_delete(&async_stop_data);
    lcm_worker_task_delete(&stop_task);
    return false;
}

static int lxc_stop(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    struct lxc_container* c = NULL;
    const char* ctr_id = NULL;
    uint32_t shutdown_timeout = 0;

    shutdown_timeout = GET_UINT32(args, CTHULHU_PLUGIN_STOP_DATA_TIMEOUT);

    ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SET_ERR("ContainerId is not set");
        goto exit;
    }

    c = lxc_container_new(ctr_id, NULL);
    if(!c) {
        SET_ERR("Failed to setup the lxc_container struct");
        goto exit;
    }
    if(!c->is_defined(c)) {
        SET_ERR("Container '%s' does not exist", ctr_id);
        goto exit;
    }
    if(!c->is_running(c)) {
        SAH_TRACEZ_INFO(ME, "the container '%s' is not running", ctr_id);
        rc = 0;
        goto exit;
    }

    if(shutdown_timeout == 0) {
        SAH_TRACEZ_WARNING(ME,
                           "the container '%s' is being forcefully terminated without "
                           "a clean shutdown, due to a 0 timeout value",
                           ctr_id);
        if(!c->stop(c)) {
            SET_ERR("Container '%s' could not be stopped", ctr_id);
            goto exit;
        }
    } else {
        if(!submit_shutdown_task(shutdown_timeout, ctr_id)) {
            SET_ERR("Container '%s' could not be stopped", ctr_id);
            goto exit;
        }
    }
    rc = 0;
exit:
    if(c) {
        lxc_container_put(c);
    }
    return rc;
}


static int lxc_remove(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    struct lxc_container* c = NULL;
    const char* ctr_id = NULL;
    char* config_file_name = NULL;
    char* config_cpy = NULL;
    char* cfg_dir = NULL;
    lxc_instance_t* inst;


    ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SET_ERR("ContainerId is not set");
        goto exit;
    }

    c = lxc_container_new(ctr_id, NULL);
    if(!c) {
        SET_ERR("Failed to setup the lxc_container struct");
        goto exit;
    }
    if(!c->is_defined(c)) {
        SET_ERR("Container '%s' does not exist", ctr_id);
        rc = 0;
        goto exit;
    }

    config_file_name = c->config_file_name(c);
    if(string_is_empty(config_file_name)) {
        SET_ERR("Could not get config filename for container %s", ctr_id);
        goto exit;
    }
    config_cpy = strdup(config_file_name);
    cfg_dir = dirname(config_cpy);
    if(strcmp(cfg_dir, ".") == 0) {
        SET_ERR("Could extract dir from %s", config_cpy);
        goto exit;
    }
    if(cthulhu_rmdir(cfg_dir) < 0) {
        SET_ERR("Could not remove dir %s", cfg_dir);
    }
    inst = cthulhu_lxc_instance_find_by_name(c->name);
    if(inst) {
        cthulhu_lxc_instance_delete(&inst);
    }

    rc = 0;
exit:
    if(c) {
        lxc_container_put(c);
    }
    free(config_file_name);
    free(config_cpy);
    return rc;
}

static cthulhu_ctr_info_t* lxc_get_ctr_info(struct lxc_container* c) {
    cthulhu_ctr_info_t* ctr_info = NULL;
    lxc_instance_t* lxc_instance = NULL;
    amxc_string_t cthulhu_data_filename;
    char* config_file_name = NULL;
    char* cfg_dir = NULL;
    char** interfaces = NULL;
    char* interface = NULL;
    char** addresses = NULL;
    char* address = NULL;
    amxc_var_t* cthulhu_data = NULL;

    amxc_string_init(&cthulhu_data_filename, 0);

    when_failed_log(cthulhu_ctr_info_new(&ctr_info), exit);
    ctr_info->ctr_id = strdup(c->name);
    ctr_info->status = cthulhu_lxc_status_from_string(c->state(c));
    lxc_instance = cthulhu_lxc_instance_find_by_name(ctr_info->ctr_id);
    if(lxc_instance) {
        lxc_instance->status = ctr_info->status;
    }
    ctr_info->pid = c->init_pid(c);
    // get interfaces
    interfaces = c->get_interfaces(c);
    if(interfaces) {
        int itf_pos = 0;
        while(interfaces[itf_pos]) {
            interface = interfaces[itf_pos++];
            cthulhu_ctr_info_itf_t* itf = NULL;
            cthulhu_ctr_info_itf_new(&itf);
            when_null_log(itf, exit);
            itf->name = strdup(interface);
            addresses = c->get_ips(c, interface, NULL, 0);
            if(addresses) {
                int addr_pos = 0;
                while(addresses[addr_pos]) {
                    address = addresses[addr_pos++];
                    amxc_llist_add_string(&itf->ips, address);
                    free(address);
                }
                free(addresses);
            }
            amxc_llist_append(&ctr_info->interfaces, &itf->it);
            free(interface);
        }
        free(interfaces);
    }
    // try to read the bundle file
    config_file_name = c->config_file_name(c);
    cfg_dir = dirname(config_file_name);
    amxc_string_setf(&cthulhu_data_filename, "%s/cthulhu.json", cfg_dir);
    free(config_file_name);
    cthulhu_data = read_config(cthulhu_data_filename.buffer);
    if(cthulhu_data) {
        const char* bundle_name = GET_CHAR(cthulhu_data, "bundle_name");
        if(bundle_name) {
            ctr_info->bundle = strdup(bundle_name);
        }
        const char* bundle_version = GET_CHAR(cthulhu_data, "bundle_version");
        if(bundle_version) {
            ctr_info->bundle_version = strdup(bundle_version);
        }
        const char* sb_id = GET_CHAR(cthulhu_data, "sb_id");
        if(sb_id) {
            ctr_info->sb_id = strdup(sb_id);
        }
        const char* create_time = GET_CHAR(cthulhu_data, "create_time");
        if(create_time) {
            ctr_info->created = strdup(create_time);
        }
    }
    amxc_var_delete(&cthulhu_data);

exit:
    amxc_string_clean(&cthulhu_data_filename);

    return ctr_info;

}

static int lxc_list(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    int num = 0;
    int i = 0;
    struct lxc_container** containers = NULL;
    const char* lxcpath = NULL;

    lxcpath = lxc_get_global_config_item("lxc.lxcpath");
    if(!cthulhu_isdir(lxcpath)) {
        SET_ERR("LXC Path [%s] does not exist", lxcpath);
        goto exit;
    }
    amxc_var_init(ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    num = list_all_containers(lxcpath, NULL, &containers);
    for(i = 0; i < num; ++i) {
        cthulhu_ctr_info_t* ctr_info = lxc_get_ctr_info(containers[i]);
        if(!ctr_info) {
            SET_ERR("Failed to get container info");
            lxc_container_put(containers[i]);
            goto exit;
        }
        amxc_var_add_new_cthulhu_ctr_info_t(ret, ctr_info);
        lxc_container_put(containers[i]);
    }

    rc = 0;
    goto exit;
exit:
    free(containers);
    return rc;
}

static void lxc_notif_changed(struct lxc_container* c) {
    amxc_var_t notif;
    amxc_var_init(&notif);
    amxc_var_set_type(&notif, AMXC_VAR_ID_HTABLE);

    cthulhu_ctr_info_t* ctr_info = lxc_get_ctr_info(c);
    when_null_log(ctr_info, exit);

    when_failed_log(amxc_var_set_cthulhu_ctr_info_t(&notif, ctr_info), exit);

    amxd_object_emit_signal(rootobject, CTHULHU_BACKEND_NOTIF_CHANGED, &notif);
exit:
    amxc_var_clean(&notif);
}

static void update_ctr_inst(lxc_instance_t* inst) {
    struct lxc_container* c = lxc_container_new(inst->ctr_name, NULL);
    if(!c) {
        goto exit;
    }
    if(!c->is_defined(c)) {
        goto exit;
    }

    cthulhu_ctr_status_t s = cthulhu_lxc_status_from_string(c->state(c));
    if(s != inst->status) {
        SAH_TRACEZ_INFO(ME, "[%s] State changed from %s to %s",
                        inst->ctr_name,
                        cthulhu_ctr_status_to_string(inst->status), cthulhu_ctr_status_to_string(s));
        lxc_notif_changed(c);
        inst->status = s;
    }
exit:
    if(c) {
        lxc_container_put(c);
    }

}

static void update_timer_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    amxc_llist_t* lxc_instances = cthulhu_lxc_get_instances();
    when_null_log(lxc_instances, exit);
    lxc_instance_t* inst = NULL;

    amxc_llist_for_each(it, lxc_instances) {
        inst = amxc_llist_it_get_data(it, lxc_instance_t, it);
        update_ctr_inst(inst);
    }

exit:
    return;
}

static int lxc_version(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    amxc_var_init(ret);
    char version[64];
    version[sizeof(version) - 1] = '\0';
    snprintf(version, sizeof(version) - 1, "lxc:%s", lxc_get_version());
    amxc_var_set_cstring_t(ret, version);
    return 0;
}

static int lxc_information(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    amxc_var_init(ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(ret, CTHULHU_INFO_BACKENDNAME, "lxc");
    amxc_var_add_new_key_bool(ret, CTHULHU_INFO_BUNDLES_SUPPORTED, false);
    amxc_var_add_new_key_bool(ret, CTHULHU_INFO_OVERLAYFSONCREATE, false);

    return 0;
}

static int lxc_init(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    parser = (amxo_parser_t*) GET_DATA(args, CTHULHU_BACKEND_CMD_INIT_PARAM_PARSER);
    rootobject = (amxd_object_t*) GET_DATA(args, CTHULHU_BACKEND_CMD_INIT_PARAM_ROOTOBJECT);

    when_null_log(parser, exit);
    when_null_log(rootobject, exit);
    if(lcm_worker_new(&lcm_worker, NULL, "lxc_worker") != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create LCM worker");
        goto exit;
    }
    if(lcm_worker_start(lcm_worker, parser) != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not start LCM worker");
        goto exit;
    }
    if(pipe2(ctr_sig_pipe, O_NONBLOCK) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create pipe");
        goto exit;
    }
    amxo_connection_add(parser, ctr_sig_pipe[0], lxc_stopped_handler, NULL, AMXO_LISTEN, NULL);
    rc = 0;
exit:
    amxc_var_set(bool, ret, rc);
    return rc;
}

static char* read_output(int fd) {
    ssize_t bytes_read = 0;
    size_t buffer_size = EXEC_OUTPUT_SIZE;
    size_t total_bytes_read = 0;
    char* buffer = NULL;

    buffer = calloc(1, buffer_size);
    ASSERT_NOT_NULL(buffer, goto error, "Cannot allocate memory for reading execution output");

    do {
        bytes_read = read(fd, buffer + total_bytes_read, EXEC_OUTPUT_SIZE);
        if(bytes_read < 0) {
            if((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                // No more data available
                break;
            } else {
                free(buffer);
                return NULL;
            }
        } else if(bytes_read < EXEC_OUTPUT_SIZE) {
            break;
        } else {
            total_bytes_read += bytes_read;
            // Reallocate buffer to increase its size
            buffer_size = buffer_size + EXEC_OUTPUT_SIZE;
            char* temp = realloc(buffer, buffer_size);
            ASSERT_NOT_NULL(temp, goto error, "Cannot allocate memory for reading execution output");
            // Initialise the newly allocated memory
            memset(temp + total_bytes_read, 0, EXEC_OUTPUT_SIZE);
            buffer = temp;
        }
    } while (bytes_read > 0);

    return buffer;

error:
    free(buffer);
    return NULL;
}

static void lxc_execute_ctr(struct lxc_container* c, const char* ctr_id,
                            int fd, amxc_var_t* args) {
    struct lxc_attach_options_t options = LXC_ATTACH_OPTIONS_DEFAULT;
    char** cmd_arguments = NULL;
    size_t args_size = 0;
    int pipefd[2], status, retval = 0;
    char* buffer = NULL;
    amxc_string_t msg;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&msg, 0);

    const char* command = GET_CHAR(args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_COMMAND);
    amxc_var_t* arguments = GETP_ARG(args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_ARGUMENTS);

    // attach_run_wait API expect cmd_arguments format: {command, "arg1", "arg2", ..., NULL };
    const amxc_llist_t* arglist = amxc_var_constcast(amxc_llist_t, arguments);
    if(arglist) {
        args_size = amxc_llist_size(arglist);
    }
    cmd_arguments = (char**) malloc((args_size + 2) * sizeof(char*));
    cmd_arguments[0] = strdup(command);
    if(args_size) {
        int i = 0;
        amxc_var_t* inst = NULL;
        amxc_llist_for_each(it, arglist) {
            inst = amxc_llist_it_get_data(it, amxc_var_t, lit);
            cmd_arguments[++i] = amxc_var_dyncast(cstring_t, inst);
        }
    }
    cmd_arguments[args_size + 1] = NULL;


    // Prepare output buffer
    if(pipe(pipefd) == -1) {
        amxc_string_setf(&msg, "Could not init the output buffer for CTR [%s]", ctr_id);
        goto sendresult;
    }
    options.stdout_fd = pipefd[1];
    int flags = fcntl(pipefd[0], F_GETFL, 0);
    fcntl(pipefd[0], F_SETFL, flags | O_NONBLOCK);

    retval = 1;
    status = c->attach_run_wait(c, &options, command, (const char* const*) cmd_arguments);
    if(status < 0) {
        amxc_string_setf(&msg, "Failed to execute command inside CTR [%s]", ctr_id);
    } else {
        if(WIFEXITED(status)) {
            if(WEXITSTATUS(status) != 0) {
                amxc_string_setf(&msg, "Command [%s] not supported or failed on CTR [%s]", command, ctr_id);
            } else {
                buffer = read_output(pipefd[0]);
                if(buffer) {
                    retval = 0;
                    amxc_string_setf(&msg, "%s", buffer);
                    free(buffer);
                } else {
                    amxc_string_setf(&msg, "Command [%s] executed succefully on CTR [%s] but output result retrieval failed", command, ctr_id);
                }
            }
        } else if(!WIFEXITED(status)) {
            amxc_string_setf(&msg, "Command [%s] not termiated properly on CTR [%s]", command, ctr_id);
        } else if(WIFSIGNALED(status)) {
            amxc_string_setf(&msg, "Command [%s] for CTR [%s] terminated by a signal [%d]", command, ctr_id, WTERMSIG(status));
        }
    }

sendresult:
    // construct output data: retval and command output or error msg
    amxc_var_set_type(&ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &ret, "retval", retval);
    amxc_var_add_key(cstring_t, &ret, "output", amxc_string_get(&msg, 0));


    ASSERT_TRUE(lcm_var_serialize(fd, &ret) > 0, NO_INSTRUCTION, "Failed to send execution result data");

    // clear all data
    amxc_var_clean(&ret);
    amxc_string_clean(&msg);
    for(int i = 0; i < (int) args_size + 1; i++) {
        free(cmd_arguments[i]);
    }
    free(cmd_arguments);

    // pause until the parent stops the process
    pause();
}

/**
   @brief
   Execute a command inside a container

   Check if the container is available and running, and then execute requested command,
   possibly with arguments, inside it.

   @param function_name cthulhu-lxc function name
   @param args A pointer a variant struct containing the command to be executed
               and its optional arguments list
   @param ret A pointer a variant struct that will hold string result msg of the
              execution

   @return
   0 when successful execution.
   1 when container execution started but did not terminate successfully (failed, timeout, result retrieval failed,..)
   2 when execution not started - container not available or not running.
 */
static int lxc_execute(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    int retval = 2;
    struct lxc_container* c = NULL;
    const char* ctr_id = NULL;
    pid_t pid;
    int pipefd[2];

    ASSERT_NOT_NULL(ret, goto exit, "Failed to get ret struct");

    ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if(string_is_empty(ctr_id)) {
        SET_ERR("ContainerId is not set");
        return retval;
    }

    c = lxc_container_new(ctr_id, NULL);
    if(!c) {
        SET_ERR("Failed to setup the lxc_container struct");
        return retval;
    }
    if(!c->is_defined(c)) {
        SET_ERR("Container '%s' does not exist", ctr_id);
        goto exit;
    }
    if(!c->is_running(c)) {
        SET_ERR("the container '%s' is not running", ctr_id);
        goto exit;
    }

    uint32_t timeout = DEFAULT_EXECUTION_TIMEOUT;
    if(!GETP_ARG(args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_TIMEOUT)) {
        SAH_TRACEZ_WARNING(ME, "Execution timeout not specified. Default of %d seconds is set", DEFAULT_EXECUTION_TIMEOUT);
    } else {
        timeout = GET_UINT32(args, CTHULHU_MOD_BACKEND_COMMAND_EXECUTE_PARAMS_TIMEOUT);
    }

    ASSERT_EQUAL(pipe(pipefd), 0, goto exit,
                 "Failed to create communication pipe: Command cannot be executed")

    pid = fork();
    if(pid == -1) {
        SAH_TRACEZ_ERROR(ME, "Failed to start the execution process");
        goto exit;
    } else if(pid == 0) { //child executor
        close(pipefd[0]);

        // Remove previously registered signal handlers
        signal(SIGINT, SIG_IGN);
        signal(SIGTERM, SIG_IGN);
        signal(SIGABRT, SIG_IGN);

        lxc_execute_ctr(c, ctr_id, pipefd[1], args);
    } else { // parent monitor
        int ready = 0;
        fd_set read_fds;
        close(pipefd[1]);
        FD_ZERO(&read_fds);
        FD_SET(pipefd[0], &read_fds);

        // Set execution timeout
        struct timeval stimeout = { .tv_sec = timeout, .tv_usec = 0 };
        retval = 1;
        ready = select(pipefd[0] + 1, &read_fds, NULL, NULL, &stimeout);
        if(ready && FD_ISSET(pipefd[0], &read_fds)) {
            amxc_var_t data;
            amxc_var_init(&data);

            if(lcm_var_deserialize(pipefd[0], &data) > 0) {
                retval = GET_UINT32(&data, "retval");
                const char* output = GET_CHAR(&data, "output");
                if(retval == 0) {
                    SET_INFO("%s", output);
                } else {
                    SET_ERR("%s", output);
                }
            } else {
                SAH_TRACEZ_ERROR(ME, "Failed to convert execution result");
                retval = 1;
            }
            amxc_var_clean(&data);
        }
        CHECK_EQUAL_LOG(INFO, ready, 0, NO_INSTRUCTION, "Command execution timeout");
        ASSERT_NOT_EQUAL(ready, -1, NO_INSTRUCTION, "Failed in select call (%d: %s)", errno, strerror(errno));

        // Stop the execution process (happen on timeout, execution end, or failure)
        kill(pid, SIGKILL);
        cthulhu_wait_for_pid(pid);
    }


exit:
    lxc_container_put(c);
    return retval;
}

AMXM_CONSTRUCTOR module_init(void) {
    SAH_TRACEZ_INFO(ME, ">>> Cthulhu_lxc constructor\n");
    int ret = 1;

    so = amxm_so_get_current();
    if(so == NULL) {
        SAH_TRACEZ_INFO(ME, "Could not get shared object\n");
        goto exit;
    }

    if(amxm_module_register(&mod, so, MOD_CTHULHU)) {
        SAH_TRACEZ_INFO(ME, "Could not register module %s\n", MOD_CTHULHU);
        goto exit;
    }

    amxp_timer_new(&update_timer, update_timer_cb, NULL);
    amxp_timer_set_interval(update_timer, 2000);
    amxp_timer_start(update_timer, 2000);

    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_INIT, lxc_init), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_INFORMATION, lxc_information), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_CREATE, lxc_create), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_START, lxc_start), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_STOP, lxc_stop), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_REMOVE, lxc_remove), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_LIST, lxc_list), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_VERSION, lxc_version), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_CTR_EXECUTE, lxc_execute), exit);

    ret = 0;
exit:
    SAH_TRACEZ_INFO(ME, "<<< Cthulhu_lxc constructor\n");
    return ret;
}

AMXM_DESTRUCTOR module_exit(void) {
    SAH_TRACEZ_INFO(ME, ">>> Cthulhu_lxc destructor\n");
    lcm_worker_stop(lcm_worker, false);
    lcm_worker_delete(&lcm_worker, false);
    cthulhu_lxc_instance_clean();
    SAH_TRACEZ_INFO(ME, "<<< Cthulhu_lxc destructor\n");
    return 0;
}
